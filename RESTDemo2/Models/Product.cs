﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RESTDemo2.Models
{
	/// <summary>
	/// Product Data
	/// </summary>
	[DataContract]
	public class Product
	{
		/// <summary>
		/// Product Id
		/// </summary>
		[DataMember(Name = "productid")]
		public Guid ProductId { get; set; }

		/// <summary>
		/// Product Name
		/// </summary>
		[DataMember(Name = "productname")]
		public string ProductName { get; set; }

		/// <summary>
		/// Product Code (Unique number of product)
		/// </summary>
		[DataMember(Name = "productnumber")]
		public string ProductNumber { get; set; }

		/// <summary>
		/// Stock of product in warehouse
		/// </summary>
		[DataMember(Name = "stock")]
		public int Stock { get; set; }

		/// <summary>
		/// Number of day of production
		/// </summary>
		[DataMember(Name = "daysTomanufacture")]
		public int DaysToManufacture { get; set; }

		public bool ActiveStatus { get; set; }
	}
}