﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RESTDemo2.DataAccessLayer;
using RESTDemo2.Models;

namespace RESTDemo2.Controllers
{
    public class ProductController : ApiController
    {
        private StorageDB db = new StorageDB();

	    /// <summary>
	    /// Get all product data
	    /// </summary>
	    /// <remarks>
	    /// Get list of all product
	    /// </remarks>
	    /// <returns></returns>
	    /// <response code="200"></response>
		// GET: api/Product
		public List<Product> GetProduct()
        {
			List<Product> datas = new List<Product>();
	        foreach (Product dt in db.Product)
	        {
		        if (dt.ActiveStatus == true)
		        {
					datas.Add(dt);
		        }
	        }
            return datas;
        }

		/// <summary>
		/// Get product data by id
		/// </summary>
		/// <remarks>
		/// Get product data by id
		/// </remarks>
		/// <returns></returns>
		/// <response code="200"></response>
		// GET: api/Product/5
		[ResponseType(typeof(Product))]
        public IHttpActionResult GetProduct(Guid id)
        {
            Product product = db.Product.Find(id);

			if (product == null || product.ActiveStatus == false)
            {
                return NotFound();
            }

            return Ok(product);
        }

	    /// <summary>
	    /// Create new product or update an existing product
	    /// </summary>
	    /// <remarks>
	    /// Create new product or update an existing product
	    /// </remarks>
	    /// <returns></returns>
	    /// <response code="200"></response>
		// PUT: api/Product/5
		[ResponseType(typeof(void))]
        public IHttpActionResult PutProduct(string keyValue)
        {
	        IQueryable<Product> datas = db.Product;
			Product prod = new Product();

	        foreach (Product dt in datas)
	        {
		        if (dt.ProductName.Contains(keyValue) || dt.ProductNumber.Contains(keyValue))
		        {
			        prod = dt;
					break;
		        }
	        }


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (keyValue != prod.ProductName && keyValue != prod.ProductNumber)
            {
                return BadRequest();
            }

            db.Entry(prod).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(keyValue))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

	    /// <summary>
	    /// Create new product data
	    /// </summary>
	    /// <remarks>
	    /// Create new product
	    /// </remarks>
	    /// <returns></returns>
	    /// <response code="200"></response>
		// POST: api/Product
		[ResponseType(typeof(Product))]
        public IHttpActionResult PostProduct(string ProductName, string ProductNumber, int Stock, int DaysToManufacture)
        {
			Product product = new Product();

            if (ProductName == String.Empty || ProductNumber == String.Empty || Stock < 0 || DaysToManufacture < 0)
            {
                return BadRequest(ModelState);
            }

			product.ProductId = Guid.NewGuid();
	        product.ProductName = ProductName;
	        product.ProductNumber = ProductNumber;
	        product.Stock = Stock;
	        product.DaysToManufacture = DaysToManufacture;
	        product.ActiveStatus = true;

            db.Product.Add(product);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = product.ProductId }, product);
        }

	    /// <summary>
	    /// Remove an existing product
	    /// </summary>
	    /// <remarks>
	    /// Remove an existing product
	    /// </remarks>
	    /// <returns></returns>
	    /// <response code="200"></response>
		// DELETE: api/Product/5
		[ResponseType(typeof(Product))]
        public IHttpActionResult DeleteProduct(Guid id)
        {
            Product product = db.Product.Find(id);
            if (product == null)
            {
                return NotFound();
            }

	        product.ActiveStatus = false;
	        db.Entry(product).State = EntityState.Modified;

			try
	        {
		        db.SaveChanges();
	        }
	        catch (DbUpdateConcurrencyException)
	        {
		        if (!ProductExists(id))
		        {
			        return NotFound();
		        }
		        else
		        {
			        throw;
		        }
	        }

	        //return StatusCode(HttpStatusCode.NoContent);
			
			//db.Product.Remove(product);
			//db.SaveChanges();

            return Ok(product);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(string keyValue)
        {
            return db.Product.Count(e => e.ProductName.Contains(keyValue) || e.ProductNumber.Contains(keyValue)  ) > 0;
        }

	    private bool ProductExists(Guid id)
	    {
		    return db.Product.Count(e => e.ProductId == id) > 0;
	    }
	}
}